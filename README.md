# README #
# Assertion Semantic Analysis #

Assertion Semantic Analysis for LBDR (Logic-Based Distributed Routing) and Arbiter

Authors: Gianluca Martino, Tara Ghasempouri, Behrad Niazmand

# Instructions: #

	* To run simulations, first you should set your environment to run QuestaSim (vsim). 

	* Go to the sim directory: 
		cd Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/sim/ (for LBDR)
		cd Assertion_semantic_analysis/Arbiter_assertion_semantic_analysis/sim/ (for Arbiter)		

	* To run simulation with valid set of input sequence, exhaustive set of input sequence, and real scenario input sequence, do the following: 
		a - For valid input: vsim -do sim_LBDR_valid.do
		b - For exhaustive input: vsim -do sim_LBDR_exhaustive.do
		c - For real scenario input: vsim -do sim_LBDR_real_scenario.do
		
	* After simulation, the corresponding assertion report will be generated in sim folder. 

	* The original set of properties with LTL syntax can be accessed in: 
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/properties_ltl/LBDR_properties_LTL.txt
		Assertion_semantic_analysis/Arbiter_assertion_semantic_analysis/properties_ltl/Arbiter_properties_LTL.txt
		
	* The automatically generated set of SystemVerilog properties and assertions by in-house script based on parsing the LTL properties can be accessed in:
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/properties_sva/LBDR_properties.sva	
		Assertion_semantic_analysis/Arbiter_assertion_semantic_analysis/properties_sva/Arbiter_properties.sva	

	* The valid and exhaustive input sequences (test vectors) can be accessed in: 
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/tb/test_vectors/input_seq.txt  (exhaustive set of input sequence) for LBDR
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/tb/test_vectors/valid_ip.txt   (valid set of input sequence) for LBDR
	  (For Arbiter, only real scenario test is defined!)	
		
	* The order of inputs in the input sequence files based on LBDR input ports is as follows (from left to right): 
	empty, Rxy_rst, Cx_rst, flit_id, dst_addr, cur_addr_rst
	1 bit, 8 bits,  4 bits, 3 bits,  4 bits,   4 bits       = 24 bits
	(clk and rst are applied via the test bench, reset is active high for this design.)
	** Important: Currently, due to the assumptions in the model checker tool, the values of 
				  Rxy_rst, Cx_rst, and cur_addr_rst are all assumed to be zero under all test-benches, 
				  however, this would not be a realistic case and later needs to be updated!)

	* For the real scenario test, the test-bench is implemented in: 
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/tb/LBDR_tb_real_scenario.sv
		Assertion_semantic_analysis/Arbiter_assertion_semantic_analysis/tb/Arbiter_tb_real_scenario.sv
		
	* For the valid inputs and exhaustive inputs of LBDR, the tasks and test-bench are implemented in: 
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/tb/tc_exhaustive_stimuli.sv
		Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/tb/LBDR_tb.sv
	(The defined switch VALID_SEQ in sim_LBDR_valid.do file, guides the simulator to infer the valid input stimuli, which is in /tb/test_vectors/valid_ip.txt, 
	 If the switch is not defined, the exhaustive set of input stimuli is inferred (as it happens when running simulator with sim_LBDR_exhaustive.do file, the test vector is in /tb/test_vectors/input_seq.txt)
	
	* It should be noted that the simulation of the exhaustive set of input stimuli can be time-consuming, taking more than 2 hours!
	
	* For more information about LBDR's behaviour and logic, please refer to the corresponding paper [2].
	--------------------------------------------
	
	* The definition of the columns corresponding to passing and failure of properties in the reports is as follows: 
	(the corresponding reports for assertions are stored in LBDR_assertion_semantic_analysis/LBDR_assertion_semantic_analysis/reports/ folder with corresponding name of the test scenario (valid, exhaustive or real scenario.)
	
		(Adopted from [1])
		1) Failure count: "The number of start attempts that resulted in failure."
		2) Pass count: "The number of start attempts when the assertion passed."
		3) Vacuous count: "The number of start attempts when the assertion passed vacuously."
		4) Disable count: "The number of start attempts that were disabled due to the disable condition being TRUE."
		5) Attempt count: "The number of times the assertion was attempted, which basically corresponds to the number of clock edges for the assertion."
		6) Active count: "The number of start attempts that are currently active."
		
		Important: "Attempt count = Failure count + Vacuous count + Pass count + Disable count + Active count"
		
	* Cover and Assertion results are available in the following formats (for all three test scenarios): 
	  (Similar for Arbiter in Arbiter_assertion_semantic_analysis/reports/ folder)
		a) txt: Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/reports/txt/
		b) xml: Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/reports/xml/
		d) csv: Assertion_semantic_analysis/LBDR_assertion_semantic_analysis/reports/csv/ (automatically generated using in-house script)

	* Scripts: 
	  In the root of the repository, the Script folder consists of: 
	    a) An LTL parser (ltl_parser.py), which parses the LTL properties generated for Counting Semantics tool and translates them into SystemVerilog Assertions (SVA) syntax, along with cover statements for checking antecedents and consequences. 
	    b) A formal property parser (formal_properties_parser.ph), which is in charge of filtering the properties that do not have any counter traces when evaulated using formal property verification tools. This script needs update and is not in stable condition yet. 
	    c) Shayan input generator (shayan_input_gen.py), which parses the cover report acquired from simulation of different test-bench scenarios and generates inputs for Shayan tool in a syntax form usable by the tool (more information about Shayan is at: /tools/Shayan/readme.txt file).

	    Examples for using the Python script for the case of LBDR are as follows: 
	    To generate Shayan inputs (Assuming Python 2.7 is at least installed on your system):  
	       1) change directory to Scripts/
	       2) Type (if using terminal or command prompt):
	         For valid input stimuli:   
	            python main.py -shayan -i ../LBDR_assertion_semantic_analysis/reports/txt/cover_report_valid_ip.txt
	         For real scenario test bench : 
	            python main.py -shayan -i ../LBDR_assertion_semantic_analysis/reports/txt/cover_report_real_scenario.txt

	    It is worth noting that LTL parser script might still need to be debugged, especially in case the design name changes. The same would apply to Shayan input generator script.

		
# Reference(s) #

	[1] Questa SIM User's Manual, Software Version 10.6c, Document Revision 2.2 (2017), [Online: https://www.mentor.com/]
	[2] J. Flich and J. Duato, "Logic-Based Distributed Routing for NoCs," in IEEE Computer Architecture Letters, vol. 7, no. 1, pp. 13-16, Jan. 2008, doi: 10.1109/L-CA.2007.16.

		
	