
from math import log
import sys


def generate_shayan_input_from_coverage_report(prop_file_name):

	if not isinstance(prop_file_name, str):
		raise ValueError(prop_file_name + "is not a string!")
	# print "// -----------------------------------------------------"
	# print " "

	prop_file = open(prop_file_name, 'r')

	dut_name = ""
	if "Arbiter" in prop_file_name: 
		dut_name = "Arbiter"
	elif "LBDR" in prop_file_name:
		dut_name = "LBDR"

	counter = 0
	prop_dict = {}
	prop_cond_dict = {}
	seq_cond_dict = {}
	prop_symp_dict = {}
	seq_symp_dict = {}
	cov_00_dict = {}
	cov_01_dict = {}
	cov_10_dict = {}
	cov_11_dict = {}
	property_number = []

	if dut_name == "LBDR":
		for line in prop_file:
			cov_00_dict[counter] = []
			if "00_cover" in line:
				#Store property number
				property_number.append(line[line.index("00_cover_")+9:line.index(" ")])
				next_line = next(prop_file)
				cov_00_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_00_cover_count = " + cov_00_dict[counter]

				counter= counter+1

		prop_file.close()

		num_of_properties = counter
		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_01_dict[counter] = []
			if "01_cover" in line:
				next_line = next(prop_file)
				cov_01_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_01_cover_count = " + cov_01_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_10_dict[counter] = []
			if "10_cover" in line:
				next_line = next(prop_file)
				cov_10_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_10_cover_count = " + cov_10_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_11_dict[counter] = []
			if "11_cover" in line:
				next_line = next(prop_file)
				cov_11_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_11_cover_count = " + cov_11_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0

	elif dut_name == "Arbiter":
		for line in prop_file:
			cov_00_dict[counter] = []
			if "00_cover" in line:
				property_number.append(int(line[line.index("00_cover_")+9:]))
				# Read 2 lines afterwards, focus is on the 2nd line!
				next_line = next(prop_file)
				next_line = next(prop_file)
				cov_00_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_00_cover_count = " + cov_00_dict[counter]

				counter= counter+1

		prop_file.close()

		num_of_properties = counter
		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_01_dict[counter] = []
			if "01_cover" in line:
				next_line = next(prop_file)
				next_line = next(prop_file)
				cov_01_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_01_cover_count = " + cov_01_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_10_dict[counter] = []
			if "10_cover" in line:
				next_line = next(prop_file)
				next_line = next(prop_file)
				cov_10_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_10_cover_count = " + cov_10_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0

		prop_file = open(prop_file_name, 'r')

		for line in prop_file:
			cov_11_dict[counter] = []
			if "11_cover" in line:
				next_line = next(prop_file)
				next_line = next(prop_file)
				cov_11_dict[counter]  = next_line.split()[0]
				# print "C_" + dut_name + "_property_" + str(counter+1) + "_11_cover_count = " + cov_11_dict[counter]
				
				counter= counter+1

		prop_file.close()

		counter = 0


	# Only for debugging!
	# print "cov_00_dict length = " + str(len(cov_00_dict))
	# print "Number of Properties = " + str(num_of_properties)


	# We want to print the cover values in csv format parsable by Shayan tool !!
	# Shayan format (order) is : 
	# 11	10	01	00	11+10	01+00	11+01	10+00	11+10+01+00

	for property_index in range(0, num_of_properties): 
		print "C_" + dut_name + "_property_" + str(int(property_number[property_index])) + "," + str(int(cov_11_dict[property_index])) + "," + str(int(cov_10_dict[property_index])) + "," + str(int(cov_01_dict[property_index])) + "," + str(int(cov_00_dict[property_index])) + "," + str( int(cov_11_dict[property_index])+int(cov_10_dict[property_index]) ) + ","  + str( int(cov_01_dict[property_index]) + int(cov_00_dict[property_index])) + "," + str( int(cov_11_dict[property_index]) + int(cov_01_dict[property_index]) ) + "," + str( int(cov_10_dict[property_index]) + int(cov_00_dict[property_index]) ) + "," + str (int(cov_11_dict[property_index]) + int(cov_10_dict[property_index]) + int(cov_01_dict[property_index]) + int(cov_00_dict[property_index]))

	failed_properties_file = open(dut_name + "_failed_properties.txt", 'w')

	num_of_failed_properties = 0
	for property_index in range(0, num_of_properties): 
		if (int(cov_10_dict[property_index]) > 0) :
			num_of_failed_properties = num_of_failed_properties + 1

	failed_properties_file.write("failed_properties_list = [ ")
	iteration = 0
	for property_index in range(0, num_of_properties): 
		if (int(cov_10_dict[property_index]) > 0) : # if property is failing, it must be removed from list of properties! 
			iteration = iteration + 1
			if iteration == num_of_failed_properties:
				# put the failed property number in list
				failed_properties_file.write(str(int(property_number[property_index])) + " ] ")
			else :
				# put the failed property number in list
				failed_properties_file.write(str(int(property_number[property_index])) + " , ")
				if (iteration % 24 == 0):
					failed_properties_file.write(str(int(property_number[property_index])) + " , \n")
	failed_properties_file.write("] \n")


	failed_properties_file.close()

	print "\nNumber of failed properties : " + str(num_of_failed_properties)

	return cov_00_dict, prop_symp_dict

