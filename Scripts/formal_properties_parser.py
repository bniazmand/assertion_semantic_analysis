
from math import log
import sys



def generate_passing_properties_from_formal_results(prop_file_name):

	if not isinstance(prop_file_name, str):
		raise ValueError(prop_file_name + "is not a string!")

	prop_file = open(prop_file_name, 'r')

	prop_cond_dict = {}
	prop_symp_dict = {}

	passing_properties_file_name = ""

	if "Arbiter" in prop_file_name: 
		passing_properties_file_name = "Arbiter" + "_passing_properties.txt"
	elif "LBDR" in prop_file_name:
		passing_properties_file_name = "LBDR" + "_passing_properties.txt"

	passing_prop_file = open(passing_properties_file_name, 'w')

	for line in prop_file:
		if "proven" in line: 
			passing_prop_file.write(line[line.index(" "):line.index("proven")] + "\n")

	print passing_properties_file_name + " file created!"

	passing_prop_file.close()
	prop_file.close()

	return prop_cond_dict, prop_symp_dict

