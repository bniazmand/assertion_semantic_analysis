import copy
import sys
from functions import *
from package import sys_arguments
from ltl_parser import *
from shayan_input_gen import *
from formal_properties_parser import *

# system prep
sys_arguments = copy.deepcopy(parse_arguments(sys.argv, sys_arguments))					# parse the user inputs


if "-ltl" in sys.argv[:]:
	prop_cond_dict, prop_symp_dict = generate_prop_dictionary_sva_from_ltl(sys_arguments["input_property_file"])
elif "-shayan" in sys.argv[:]:
	prop_cond_dict, prop_symp_dict = generate_shayan_input_from_coverage_report(sys_arguments["input_property_file"])
elif "-formal" in sys.argv[:]:
	prop_cond_dict, prop_symp_dict = generate_passing_properties_from_formal_results(sys_arguments["input_property_file"])
else: 
	raise ValueError ("Invalid command!")