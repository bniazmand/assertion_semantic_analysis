Assertion report generated on: 29-Mar-2021 12:48:34 AM

Assertion summary report for design: ibex_LBDR
##############################################

Target | Total | Passed | Failed | Vacuous | Hit | Miss
------ | ----- | ------ | ------ | ------- | --- | ----
Eport  |   4   |   0    |   0    |    4    | 0.0 | 1.0 
Lport  |   4   |   0    |   0    |    4    | 0.0 | 1.0 
Nport  |   4   |   0    |   0    |    4    | 0.0 | 1.0 
Sport  |  N/A  |  N/A   |  N/A   |   N/A   | N/A | N/A 
Wport  |  N/A  |  N/A   |  N/A   |   N/A   | N/A | N/A 


Resource usage summary report for the desing: ibex_LBDR
#######################################################

      Phase       | Time (in [H]H:MM:SS:UUUUUU) | Memory (in MB)
----------------- | --------------------------- | --------------
V Parse & Ranking |       0:00:01.439913        |     413.81    
   Simulation     |       0:00:02.029318        |     486.06    
     D Parse      |       0:00:06.367914        |     488.05    
     Mining       |       0:00:12.375742        |     503.36    
     Overall      |       0:00:39.592949        |     503.36    
