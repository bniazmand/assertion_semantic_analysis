# create the Library working directory
vlib work


# compile the src and tb files along with the includes and options
vlog -work work -vopt +incdir+../include -nocovercells "../rtl/LBDR.sv"
vlog -work work -vopt +incdir+../include -nocovercells "../tb/LBDR_tb.sv" -assertdebug -cover bcefsx +define+VALID_SEQ

# simulate the top file(testbench)
vsim -assertdebug -t 1ns -coverage -voptargs="+cover=bcestfx" work.LBDR_tb +define+VALID_SEQ

# View Assertions
view assertions

# add the signals into waveform
add wave sim/:LBDR_tb:*

vcd file wave_dumps_valid_ip.vcd
vcd add -r -optcells LBDR_tb:DUT:*	

#run the simulation
run -all

vcd flush

# txt reports
coverage report -assert -detail -verbose -file assertion_report_valid_ip.txt -r :
coverage report -detail -cvg -directive -comments -file cover_report_valid_ip.txt -r :

# xml reports
coverage report -assert -detail -verbose -xml -file assertion_report_valid_ip.xml -r :
coverage report -detail -cvg -directive -comments -xml -file cover_report_valid_ip.xml -r :

