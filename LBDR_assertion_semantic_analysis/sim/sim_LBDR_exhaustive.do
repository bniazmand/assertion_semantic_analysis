# create the Library working directory
vlib work

# compile the src and tb files along with the includes and options
vlog -work work -vopt +incdir+../include -nocovercells "../rtl/LBDR.sv"
vlog -work work -vopt +incdir+../include -nocovercells "../tb/LBDR_tb.sv" -assertdebug -cover bcefsx

# simulate the top file(testbench)
vsim -assertdebug -t 1ns -coverage -voptargs="+cover=bcestfx" work.LBDR_tb

# View Assertions
view assertions

# add the signals into waveform
add wave sim/:LBDR_tb:*
add wave :LBDR_tb:LBDR_assertion_1 :LBDR_tb:LBDR_assertion_2 :LBDR_tb:LBDR_assertion_3 :LBDR_tb:LBDR_assertion_4 :LBDR_tb:LBDR_assertion_5 :LBDR_tb:LBDR_assertion_6 :LBDR_tb:LBDR_assertion_7 :LBDR_tb:LBDR_assertion_8 :LBDR_tb:LBDR_assertion_9 :LBDR_tb:LBDR_assertion_10 :LBDR_tb:LBDR_assertion_11 :LBDR_tb:LBDR_assertion_12 :LBDR_tb:LBDR_assertion_13 :LBDR_tb:LBDR_assertion_14 :LBDR_tb:LBDR_assertion_15 :LBDR_tb:LBDR_assertion_16  
add wave :LBDR_tb:LBDR_assertion_17 :LBDR_tb:LBDR_assertion_18 :LBDR_tb:LBDR_assertion_19 :LBDR_tb:LBDR_assertion_20

vcd file wave_dumps_exhaustive_ip.vcd
vcd add -r -optcells LBDR_tb:DUT:*

#run the simulation
run -all

vcd flush

# txt reports
coverage report -assert -detail -verbose -file assertion_report_exhaustive_ip.txt -r :
coverage report -detail -cvg -directive -comments -file cover_report_exhaustive_ip.txt -r :

# xml reports
coverage report -assert -detail -verbose -xml -file assertion_report_exhaustive_ip.xml -r :
coverage report -detail -cvg -directive -comments -xml -file cover_report_exhaustive_ip.xml -r :
