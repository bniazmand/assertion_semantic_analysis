/********************
* Filename:     LBDR_tb.v
* Description:  Testbench of LBDR with minimal support only (no deroutes and no forks). The reg is 8 routing bits, two per wire port, and 4 connectivity bits, one per wire port
* $Author:      Ranga $
*********************/

`include "../include/parameters.sv"
`include "../rtl/LBDR.sv"

//`define LBDR_assertion_test

module LBDR_tb;
   
   // Declaring the port variables for DUT
   logic clk, rst;
   logic empty = 0;
   logic [7:0] Rxy_rst;
   logic [3:0] Cx_rst;
   logic [2:0] flit_id;
   logic [3:0] dst_addr, cur_addr_rst;
   
   logic Nport, Wport, Eport, Sport, Lport;
   
   // Declaring the port variables for CHECKERS logic
 // `ifdef lbdr_assertion_test

   wire [1:0] E_validLBDRout_test;
     bit [1:0] LBDR_property_1_count;

   
   // Declaring the parameter for testcase   
   parameter test_id = 1; 

   // Declaring the local variables
   reg dut_error; // Keeps tracks of error occurred in DUT
//   event reset_trigger; // When triggered, it reset the DUT with default values
//   event reset_done; // Triggered when DUT reset is done and initialized the values  
   event terminate_sim; // When triggered, it terminates the simulation
   
   // Specify the CYCLE parameter
   parameter CYCLE = 10;
   
   // Include the task directives
   `include "./internal_task.sv"
   
   // Include the test cases
   `include "./tc_exhaustive_stimuli.sv"

   // Generating Clock of period 10ns
   initial begin
      clk = 0;
      forever
      #(CYCLE/2) clk = ~clk;
   end

   initial begin
	   clk= 0;
	   rst = 0;
	   empty = 0;
	   Rxy_rst = 0;
	   Cx_rst = 0;
	   flit_id = 0;
	   dst_addr = 0;
	   cur_addr_rst = 0;	   
	   Nport = 0;
	   Wport = 0;
	   Eport = 0;
	   Sport = 0;
	   Lport = 0;   
	end

   initial begin
	   forever begin
		   @(posedge clk) begin
		      $display("TIME:%0t *********STATUS:: clk:%0b rst:%0b empty:%0b Rxy_rst:%8b Cx_rst:%4b flit_id:%3b dst_addr:%4b cur_addr_rst:%4b Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, clk, rst, empty, Rxy_rst, Cx_rst, flit_id, dst_addr, cur_addr_rst, Nport, Eport, Wport, Sport, Lport);
		   end
		   end
   end
   
   
   // Instantiate LBDR DUT
   LBDR DUT (clk, rst,
             empty,
             Rxy_rst, Cx_rst,
             flit_id, dst_addr, cur_addr_rst,
             Nport, Eport, Wport, Sport, Lport
      );
   
   // Initializing default values for DUT
   initial begin
      $display ("###################################################");
      empty = 0;
      Rxy_rst = 8'b00011000;
      Cx_rst = 4'b1111;
      flit_id = 0; 
      dst_addr = 0; 
      cur_addr_rst = 4'b0101;
      dut_error = 0;
   end
   
   // END OF SIMULATION LOGIC
   // When simulation is terminated, it checks the dut_error and detects whether the simulation is PASSED OR NOT automatically
   initial @ (terminate_sim)  begin
      $display ("Terminating simulation");
      $display("DUT_ERROR:%0b", dut_error);
      if (dut_error == 0) begin
         $display ("Simulation Result : PASSED");
      end else begin
         $display ("Simulation Result : FAILED");
      end
      $display ("###################################################");
      #1 $stop;
   end      

   // MONITOR LOGIC
   // <!Insert monitor logic to signal of interest>
   initial begin
      $monitor("TIME:%0t *********STATUS:: Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, Nport, Eport, Wport, Sport, Lport);
   end

   initial $assertvacuousoff(0);
      
   // CHECKERS LOGIC
   // <!Instantiate checker logic>
   // LBDR_assertion_test checkers(clk, rst,
   //         empty,
   //         // Rxy_rst, Cx_rst, 
   //         flit_id, dst_addr, 
      // cur_addr_rst, 
  //    Nport, Wport, Eport, Sport, Lport);

   
   // ERROR LOGIC -- Trigger Error and terminate the simulation
   // <! If checker logic triggers a error, then the simulation
   // can be terminated by setting dut_error to '1' and calling terminate_sim event


   // RUNNING TESTCASES
   initial begin : SIM
      $display("Test started");
      
      reset();           
     // #(CYCLE * 2);
     #(CYCLE); 
     tc_exhaustive_stimuli();
             
      // END SIMULATION 
//      #(CYCLE * 2);
      #(CYCLE);
      -> terminate_sim;
   end

  // Properties and Assertions and Coverage Directives (for Shayan tool)
  // These should be auto-generated using the script !!

  `include "../properties_sva/LBDR_properties.sva"


endmodule // LBDR_tb
