/********************
* Filename:     tc_exhaustive_stimuli.v
* Description:  To test all possible input values in a exhaustive manner
* $Author:      Ranga $
*********************/


//`define VALID_SEQ -- Switch to select valid input pattern or exhaustive input pattern

`include "../include/parameters.sv"
`include "../rtl/LBDR.sv"

task tc_exhaustive_stimuli;
   parameter N_INPUT_BITS = 24;
   integer input_seq;
   integer scan_data;
   integer status;
   
   begin
      $display ("Running tc_exhaustive_stimuli ()");
`ifdef VALID_SEQ
      input_seq  = $fopen("../tb/test_vectors/valid_ip.txt", "r");
`else
      input_seq  = $fopen("../tb/test_vectors/input_seq.txt", "r");
`endif // VALID_SEQ

      if (input_seq  == 0) begin
         $display("data_file handle was NULL");
         $stop;
      end
      
      while ( !$feof(input_seq)) begin
         fork
            begin
               status = $fscanf(input_seq, "%24b \n", scan_data);
               {empty, Rxy_rst, Cx_rst, flit_id, dst_addr, cur_addr_rst} = scan_data;
				// Temporary, due to the formal tool's assumptions
				// Rxy_rst = 0;
				// Cx_rst = 0;
				// cur_addr_rst = 0;

               #(CYCLE);
            end
         join
      end

      repeat (2) begin // repeat last input sequence two more times to make total number of cases 100 
                       // (since out of 1000 cycles, 2 cycles are spent in the beginning for reset.)
         {empty, Rxy_rst, Cx_rst, flit_id, dst_addr, cur_addr_rst} = scan_data;
          #(CYCLE);
      end
      $fclose(input_seq);
   end

endtask // tc_exhaustive_stimuli

   
