/********************
* Filename:     LBDR_tb.v
* Description:  Testbench of LBDR with minimal support only (no deroutes and no forks). The reg is 8 routing bits, two per wire port, and 4 connectivity bits, one per wire port
* $Author:      Ranga $
*********************/

`include "../include/parameters.sv"
`include "../rtl/LBDR.sv"
// `include "../LBDR_assertion_test.sv"

//`define LBDR_assertion_test

module LBDR_tb;
   
   // Declaring the port variables for DUT
   logic clk, rst;
   logic empty;
   logic [7:0] Rxy_rst;
   logic [3:0] Cx_rst;
   logic [2:0] flit_id;
   logic [3:0] dst_addr, cur_addr_rst;
   
   logic Nport, Wport, Eport, Sport, Lport;
   
   // Declaring the parameter for testcase   
   parameter test_id = 1; 

   // Declaring the local variables
   event terminate_sim; // When triggered, it terminates the simulation
   
   // Specify the CYCLE parameter
   parameter CYCLE = 10;
   

   // Generating Clock of period 10ns
   initial begin
      clk = 0;
      forever
      #(CYCLE/2) clk = ~clk;
   end
   
   initial begin
      clk = 0;
      rst = 0;
      empty = 0;
      Rxy_rst = 0;
      Cx_rst = 0;
      flit_id = 0;
      dst_addr = 0;
      cur_addr_rst = 0;
   
      Nport = 0;
      Wport = 0;
      Eport = 0;
      Sport = 0;
      Lport = 0;
   end

    initial begin
     forever begin
       @(posedge clk) begin
          $display("TIME:%0t *********STATUS:: clk:%0b rst:%0b empty:%0b Rxy_rst:%8b Cx_rst:%4b flit_id:%3b dst_addr:%4b cur_addr_rst:%4b Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, clk, rst, empty, Rxy_rst, Cx_rst, flit_id, dst_addr, cur_addr_rst, Nport, Eport, Wport, Sport, Lport);
       end
     end
   end

   // Instantiate LBDR DUT
   LBDR DUT (clk, rst,
             empty,
             Rxy_rst, Cx_rst,
             flit_id, dst_addr, cur_addr_rst,
             Nport, Eport, Wport, Sport, Lport
      );
 
  task initialize_to_zero;
    begin
      rst = 1;
      @(negedge clk) begin
        Rxy_rst = 0;
        Cx_rst = 0;
        cur_addr_rst = 0;
        flit_id = 0;
        empty = 0;
      end
      @(negedge clk);
      rst = 0;
      @(negedge clk);

    end
  endtask; 
   
  // Task to generate reset and assign default settings -- Rxy, Cx, curr_addr
  task reset;
    input [7 : 0] Rxy;
    input [3 : 0] Cx;
    input [3 : 0] cur_addr;
    begin
      rst      = 1;
      @(negedge clk) begin
        Rxy_rst      = Rxy;
        Cx_rst       = Cx;
        cur_addr_rst = cur_addr;

    // Temporarily due to formal tool's assumptions
    // Rxy_rst = 0;
    // Cx_rst = 0;
    // cur_addr_rst = 0;

        flit_id      = 0;
        empty        = 1;
      end
      repeat(2)
        @(negedge clk);
      // $display("TIME:%0t HARD_RESET:: Rxy_rst: %0b, Cx_rst:%0b, cur_addr_rst:%0d", $time, Rxy_rst, Cx_rst, cur_addr_rst);
      // $display("TIME:%0t HARD_RESET:: Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, Nport, Eport, Wport, Sport, Lport);
      rst = 0;
    end
  endtask
  
  // Task to assign dst_addr along with the HEADER flit
  task flit_addr;
    input [3 : 0] d_addr;
    input [2 : 0] flit_in;
    begin
      @(negedge clk) begin
        dst_addr = d_addr;
        flit_id  = flit_in;
        // $display("TIME:%0t TASK WRITE: Flit:%0b, d_addr:%0h \n", $time, flit_in, d_addr);
      end
    end
  endtask
  
  // Task to control empty signal
  task set_empty;
    input empty_sig;
    begin
      @(negedge clk) begin
        empty = empty_sig;
      end
    end
  endtask
  
  function int coord_to_node_id(int x, int y);
    int node_id;

    node_id = (y * 4) + x;

    return node_id;
  endfunction : coord_to_node_id


  // Start the simulation
  initial begin : SIM
    integer i, i_curr, i_dst;
    integer j_curr, j_dst;
    bit [3:0] curr_node, dst_node;
    bit Cn, Ce, Cw, Cs;
    
    initialize_to_zero();

    // Reset & Initialize
    // Each node sending to other nodes in a 4x4 2D Mesh (Node IDs from 0 to 15) using XY routing
    // Each node cannot send itself!!
    // Connectivity bits order: 
    // wire Cn = Cx[0];
    // wire Ce = Cx[1];
    // wire Cw = Cx[2];
    // wire Cs = Cx[3];
  

    for(i_curr = 0; i_curr < 4; i_curr = i_curr + 1) begin // x_curr
      for(j_curr = 0; j_curr < 4; j_curr = j_curr + 1) begin // y_curr
        for(i_dst = 0; i_dst < 4; i_dst = i_dst + 1) begin // x_dst
          for(j_dst = 0; j_dst < 4; j_dst = j_dst + 1) begin // y_dst

            curr_node = coord_to_node_id(i_curr, j_curr);
            dst_node = coord_to_node_id(i_dst, j_dst);

            Cn = 1;
            Ce = 1;
            Cw = 1;
            Cs = 1;

            if (i_curr == 0)
              Cw = 0;
            else if (i_curr == 3)
              Ce = 0;
            if (j_curr == 0)
              Cn = 0;
            else if (j_curr == 3)
              Cs = 0;

            if (curr_node != dst_node) begin
              reset(8'b00111100, {Cs, Cw, Ce, Cn}, curr_node); 

              flit_addr(dst_node, `HEADER);

              repeat(2)
                set_empty(1);
              set_empty(0);

              for(i = 0; i < 4; i = i + 1) begin
                @(negedge clk) begin
                  flit_addr({$random}, `PAYLOAD);
                end
              end    

              flit_addr({$random}, `TAIL);

              repeat(2)
                set_empty(1);
            end // if (curr_node != dst_node)

          end //  y_dst
        end // x_dst
      end // y_curr
    end // x_curr


    // #(CYCLE * 5); 
    #(CYCLE); 
    $stop;
  end

 // initial begin
 //   $monitor("TIME:%0t *********STATUS:: Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, Nport, Eport, Wport, Sport, Lport);
 // end


  // Properties and Assertions and Coverage Directives (for Shayan tool)
  // These should be auto-generated using the script !!

  `include "../properties_sva/LBDR_properties.sva"



endmodule // LBDR_tb

