/********************
* Filename:     internal_task.v
* Description:  Container of all the task defined in TB
* $Author:      Ranga $
*********************/

 // Task to generate reset 
 task reset;
    begin
       rst      = 1;
       repeat(2)
          @(negedge clk);
       $display("TIME:%0t HARD_RESET:: Rxy_rst: %0b, Cx_rst:%0b, cur_addr_rst:%0d", $time, Rxy_rst, Cx_rst, cur_addr_rst);
       $display("TIME:%0t HARD_RESET:: Nport:%0b, Eport:%0b, Wport:%0b, Sport:%0b, Lport:%0b", $time, Nport, Eport, Wport, Sport, Lport);
       rst = 0;
    end
    
 endtask // reset
 