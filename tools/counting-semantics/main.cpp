#include <counting_semantics/ltl_evaluator.hpp>
#include <counting_semantics/utility/vcd_parser.hpp>
#include <counting_semantics/utility/enumerate.hpp>
#include <counting_semantics/utility/split_ltl.hpp>
#include <counting_semantics/utility/to_bool.hpp>

#include <CLI11.hpp>
#include <json.hpp>
#include <spot/tl/parse.hh>
#include <indicators.hpp>

#include <iostream>
#include <limits>

int main(int argc, char **argv) {
  CLI::App app{"Counting semantics solver"};

  auto shayan_group = app.add_option_group("Shayan", "Shayan mode options");
  auto ltl_group = app.add_option_group("LTL input", "LTL formulas options");

  std::string filename;
  app.add_option("-t,--trace", filename, "VCD trace file")->required()->check(CLI::ExistingFile);

  bool show_scopes = false;
  ltl_group->add_flag("--show-scopes", show_scopes, "Shows the scopes of the VCD trace file and exits");

  std::optional<int> trace_start_pos;
  auto trace_start_pos_option = app.add_option<std::optional<int>, int>("--trace-start-pos", trace_start_pos, "Select a starting timestep in the VCD trace. The timestep selected will be included for the evaluation")->expected(0, std::numeric_limits<int>::max());

  std::optional<int> trace_end_pos;
  auto trace_end_pos_option = app.add_option<std::optional<int>, int>("--trace-end-pos", trace_end_pos, "Select an ending timestep in the VCD trace. The timestep selected will NOT be included for the evaluation")->expected(1, std::numeric_limits<int>::max());

  bool shayan = false;
  auto shayan_opt = shayan_group->add_flag("--shayan", shayan, "Enables Shayan mode");

  std::string ltl_formula;
  ltl_group->add_option("-l,--ltl", ltl_formula, "LTL formula");

  std::string ltl_file;
  ltl_group->add_option("-f,--file", ltl_file, "File containing LTL formulas")->needs(shayan_opt)->check(CLI::ExistingFile);

  std::string output_filename = "results";
  shayan_group->add_option("-o,--output", output_filename, "Results filename. The program will generate 2 files: [filename].json and [filename].shayan. Default: results")->needs(shayan_opt);

  int pos = 0;
  app.add_option("-p,--pos", pos, fmt::format("The trace position in which the formula will be evaluated. If the options {} or {} are used, the position will refer to the interval specified", trace_start_pos_option->get_name(), trace_end_pos_option->get_name()))->expected(0, std::numeric_limits<int>::max())->excludes(shayan_opt);

  int verbose = 0;
  app.add_option("-v,--verbose", verbose, "Verbosity level")->expected(0, 100);

  std::string scope;
  app.add_option("-s,--scope", scope, "Scope to select in the VCD trace");

  bool allow_exceeding = false;
  app.add_flag("--allow-exceeding", allow_exceeding, "Using this flag, the evaluator will not automatically adjust the evaluation position to the last time step in case the position is outside of the trace limits");

  bool last_clock_cycle_next = false;
  app.add_flag("--commercial-semantics", last_clock_cycle_next, "Uses the same semantics as the commercial tools. Return false at the last cycle if the property contains NEXT");

  std::string edge;
  app.add_option("-e,--sampling-edge", edge, "Select sampling edge. Options: positive, negative")->check(CLI::IsMember({"positive", "negative"}, CLI::ignore_case));

  ltl_group->require_option(1);

  CLI11_PARSE(app, argc, argv);

  if (verbose == 1)
  {
    spdlog::set_level(spdlog::level::debug);
  }
  else if (verbose >= 2)
  {
    spdlog::set_level(spdlog::level::trace);
  }

  if (SPDLOG_ACTIVE_LEVEL < verbose) {
    spdlog::warn("Compile time verbosity level is too low to show all the warnings requested");
  }

  counting_semantics::vcd_parser parser(filename);
  auto scopes = parser.get_scopes();
  spdlog::info("Available scopes:");
  for (const auto &s : scopes)
  {
    spdlog::info(" - {} ({})", s, counting_semantics::join(parser.get_signals(s)));
  }

  if (show_scopes) {
    return 0;
  }

  auto signals = parser.get_signals(scopes[0]);

  if (scope.empty())
  {
    int i = 1;
    while (signals.empty())
    {
      signals = parser.get_signals(scopes[i++]);
    }
    spdlog::info("Selected scope: {}", scopes[--i]);
  }
  else
  {
    signals = parser.get_signals(scope);
  }

  //  signals.erase(std::remove_if(signals.begin(), signals.end(), [](auto s){ return s->size == 1; }), signals.end());

  //TODO: verify that all signals in the formula are part of the trace

  auto t = parser.to_trace(signals);
  if (edge == "positive") {
    t = t.positive_edge("clk");
  }
  else if (edge == "negative") {
    t = t.negative_edge("clk");
  }

  if (trace_start_pos.has_value() && (trace_start_pos.value() > t.size())) {
    spdlog::warn("trace_start_pos: {}, size: {}", trace_start_pos.value(), t.size());
    spdlog::warn("Ignoring invalid trace_start_pos parameter");
    trace_start_pos.reset();
  }

  if (trace_end_pos.has_value() && (trace_end_pos.value() > t.size())) {
    spdlog::warn("trace_end_pos: {}, size: {}", trace_end_pos.value(), t.size());
    spdlog::warn("Ignoring invalid trace_end_pos parameter");
    trace_end_pos.reset();
  }

  if (trace_start_pos.has_value() && trace_end_pos.has_value()) {
    t.trim(trace_start_pos.value(), t.size() - trace_end_pos.value());
  }
  else if (trace_start_pos.has_value() && !trace_end_pos.has_value()) {
    t.trim(trace_start_pos.value());
  }
  else if (!trace_start_pos.has_value() && trace_end_pos.has_value()) {
    t.trim(0, t.size() - trace_end_pos.value());
  }

  counting_semantics::ltl_evaluator evaluator;

  if (shayan)
  {
    std::vector<std::string> ltl_formulas;
    if (ltl_formula.empty()) {
      // read file and fill vector
      std::ifstream file_in(ltl_file);

      std::string line;
      while (std::getline(file_in, line))
      {
        ltl_formulas.emplace_back(line);
      }
    }
    else
    {
      ltl_formulas.emplace_back(ltl_formula);
    }

    indicators::BlockProgressBar bar{
        indicators::option::BarWidth{80},
        indicators::option::ForegroundColor{indicators::Color::white},
        indicators::option::FontStyles{std::vector<indicators::FontStyle>{indicators::FontStyle::bold}},
        indicators::option::MaxProgress{ltl_formulas.size()}
    };

    nlohmann::json results;
    std::unordered_map<std::string, std::vector<counting_semantics::bool5>> dictonary;

    for (auto [formula_id, formula] : counting_semantics::enumerate(ltl_formulas))
    {
      spot::formula f = spot::parse_formula(formula);

      nlohmann::json j;

      unsigned tt = 0;
      unsigned tf = 0;
      unsigned ft = 0;
      unsigned ff = 0;
      unsigned inconclusive = 0;
      unsigned presumably = 0;

      auto splitted = counting_semantics::split_ltl(f);
      spdlog::debug("Checking {} and {}", splitted.first, splitted.second);

      auto results_1 = evaluator.evaluate(splitted.first, t);
      auto results_2 = evaluator.evaluate(splitted.second, t);
      assert(results_1.size() == t.size());
      assert(results_2.size() == t.size());

      for (int i = 0; i < t.size(); ++i)
      {
        const auto& result_1 = results_1[i];
        if (result_1 == counting_semantics::bool5::inconclusive())
        {
          inconclusive++;
          continue;
        }
        auto& result_2 = results_2[i];
        if (result_2 == counting_semantics::bool5::inconclusive())
        {
          if (last_clock_cycle_next && i == t.size() - 1 && splitted.second.is(spot::op::X))
          {
            result_2 = counting_semantics::bool5(false);
          }
          else
          {
            inconclusive++;
            continue;
          }
        }
        if (result_1 == counting_semantics::bool5::presumably_true() || result_1 == counting_semantics::bool5::presumably_false() || result_2 == counting_semantics::bool5::presumably_true() || result_2 == counting_semantics::bool5::presumably_false())
        {
          presumably++;
        }
        if (to_bool(result_1) && to_bool(result_2))
        {
          tt++;
        }
        else if (to_bool(result_1) && !to_bool(result_2))
        {
          tf++;
        }
        else if (!to_bool(result_1) && to_bool(result_2))
        {
          ft++;
        }
        else//if (!to_bool(result_1) && !to_bool(result_2))
        {
          ff++;
        }
      }

      j["formula_id"] = formula_id + 1;
      j["tt"] = tt;
      j["tf"] = tf;
      j["ft"] = ft;
      j["ff"] = ff;
      j["inconclusive"] = inconclusive;
      j["presumably"] = presumably;
      results.emplace_back(j);

      // update progress bar
      bar.tick();
    }

    bar.mark_as_completed();

    std::ofstream json_out(output_filename + ".json");
    json_out << std::setw(4) << results << std::endl;
    spdlog::info("JSON result written to {}.json", output_filename);

    std::ofstream shayan_out(output_filename + ".shayan");
    for (const auto& [i, item] : counting_semantics::enumerate(results))
    {
      unsigned tt = item["tt"];
      unsigned tf = item["tf"];
      unsigned ft = item["ft"];
      unsigned ff = item["ff"];
      shayan_out << output_filename + "_" + std::to_string(i + 1) << ","
                 << tt << ","
                 << tf << ","
                 << ft << ","
                 << ff << ","
                 << tt + tf << ","
                 << ft + ff << ","
                 << tt + ft << ","
                 << tf + ff << ","
                 << tt + tf + ft + ff << "\n";
    }
    spdlog::info("Shayan result written to {}.shayan", output_filename);
  }
  else
  {
    if (!allow_exceeding && t.size() < pos) {
      pos = t.size() - 1;
      spdlog::info("The selected position is outside of the trace limits. Max pos: {}.", t.size());
    }
    spdlog::info("Evaluation position in trace: {}", pos);
    spot::formula f = spot::parse_formula(ltl_formula);
    auto result_0 = evaluator.evaluate(f, t, pos);
    spdlog::info("The result is {}", result_0.to_string());
    //    auto expected_result_0 = counting_semantics::bool5(true);
    //    assert(result_0 == expected_result_0);
  }

  // Show cursor
  indicators::show_console_cursor(true);

  return 0;
}
