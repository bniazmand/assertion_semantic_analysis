set(SPOT_PATH "${PROJECT_SOURCE_DIR}/lib/spot/spot-2.9.5")

include(ExternalProject)
externalProject_add(spot
        URL ${CMAKE_CURRENT_SOURCE_DIR}/spot-2.9.5.tar.gz
        URL_MD5 516fc97644bf8c40c03b54b0928a1bbe
        CONFIGURE_COMMAND ./configure --prefix=${SPOT_PATH} --enable-c++17 --enable-silent-rules
        BUILD_COMMAND make
        BUILD_IN_SOURCE 1
        INSTALL_COMMAND make install)