# counting-semantics

## Compiling

```commandline
mkdir build
cd build
cmake ..
make
```

For compiling a Release build use:
```commandline
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```


## Usage

```commandline
Counting semantics solver
Usage: ./counting_semantics_example [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  -t,--trace TEXT:FILE REQUIRED
                              VCD trace file
  --trace-start-pos INT       Select a starting timestep in the VCD trace. The timestep selected will be included for the evaluation
  --trace-end-pos INT         Select an ending timestep in the VCD trace. The timestep selected will NOT be included for the evaluation
  -p,--pos INT Excludes: --shayan
                              The trace position in which the formula will be evaluated. If the options --trace-start-pos or --trace-end-pos are used, the position will refer to the interval specified
  -v,--verbose INT            Verbosity level
  -s,--scope TEXT             Scope to select in the VCD trace
  --allow-exceeding           Using this flag, the evaluator will not automatically adjust the evaluation position to the last time step in case the position is outside of the trace limits
  -e,--sampling-edge TEXT:{positive,negative}
                              Select sampling edge. Options: positive, negative
[Option Group: Shayan]
  Shayan mode options
  Options:
    --shayan Excludes: --pos    Enables Shayan mode
    -o,--output TEXT Needs: --shayan
                                Results filename. The program will generate 2 files: [filename].json and [filename].shayan. Default: results
[Option Group: LTL input]
  LTL formulas options
  [Exactly 1 of the following options is required]
  Options:
    --show-scopes               Shows the scopes of the VCD trace file and exits
    -l,--ltl TEXT               LTL formula
    -f,--file TEXT:FILE Needs: --shayan
                                File containing LTL formulas
```

## Examples

```commandline
./counting_semantics_example -t "../test/vcd_files/wave_dumps_real_scenario.vcd" -l "G(Rxy_rst__4___ -> Lport)" --trace-start-pos=20 -p 0
```

```commandline
./counting_semantics_example --shayan -o "result" -t "../test/vcd_files/wave_dumps_real_scenario.vcd" -f "../test/ltl_properties/LBDR_properties_LTL.txt" --trace-start-pos=20 --trace-end-pos=45 -e positive
```