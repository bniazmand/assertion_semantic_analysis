cmake_minimum_required(VERSION 3.10)
project(counting_semantics)

set(CMAKE_CXX_STANDARD 17)

#add_compile_options(-g -fno-omit-frame-pointer)

add_subdirectory(include)
add_subdirectory(lib)
add_subdirectory(test)

add_executable(counting_semantics_example main.cpp)
target_include_directories(counting_semantics_example INTERFACE counting_semantics)
target_link_libraries(counting_semantics_example counting_semantics vcd-parser cli11 json indicators)
target_compile_definitions(counting_semantics_example PRIVATE
        TEST_LOCATION="${PROJECT_SOURCE_DIR}/test"
        SPDLOG_ACTIVE_LEVEL=SPDLOG_LEVEL_DEBUG)