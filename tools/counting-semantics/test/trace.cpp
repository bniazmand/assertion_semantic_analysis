//
// Created by Gianluca on 02/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#include <catch2/catch.hpp>
#include <counting_semantics/trace.hpp>

TEST_CASE( "Size of empty trace", "[trace]" )
{
  counting_semantics::trace t({});
  CHECK(t.empty());
}

TEST_CASE( "Single signal", "[trace]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);
  CHECK(t.get_signal("a") == signal);
}