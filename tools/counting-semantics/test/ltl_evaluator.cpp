//
// Created by Gianluca on 02/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch.hpp>
#include <spot/tl/parse.hh>
#include <counting_semantics/ltl_evaluator.hpp>
#include <counting_semantics/utility/vcd_parser.hpp>
#include <counting_semantics/utility/split_ltl.hpp>
#include <counting_semantics/utility/to_bool.hpp>
#include <fstream>

TEST_CASE( "Atomic predicate", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("a");

  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);

  counting_semantics::ltl_evaluator evaluator;
  auto result = evaluator.evaluate_formula(f, t, 0);
  CHECK(result.first == counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0));
}

TEST_CASE( "True", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("true");
  REQUIRE(f.is(spot::op::tt));
  spot::formula f_test = spot::parse_formula("!a || a");
  REQUIRE(!f_test.is(spot::op::tt));


  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);

  counting_semantics::ltl_evaluator evaluator;
  auto result = evaluator.evaluate_formula(f, t, 0).first;
  auto expected_result = evaluator.evaluate_formula(f_test, t, 0).first;
  CHECK(result == expected_result);
}

TEST_CASE( "False", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("false");
  REQUIRE(f.is(spot::op::ff));
  spot::formula f_test = spot::parse_formula("!(!a || a)");
  REQUIRE(!f_test.is(spot::op::ff));


  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);

  counting_semantics::ltl_evaluator evaluator;
  auto result = evaluator.evaluate_formula(f, t, 0).first;
  auto expected_result = evaluator.evaluate_formula(f_test, t, 0).first;
  CHECK(result == expected_result);
}

TEST_CASE( "Not", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("!a");

  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);

  counting_semantics::ltl_evaluator evaluator;
  auto result = evaluator.evaluate_formula(f, t, 0);
  auto expected_result = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0).swap();
  CHECK(result.first == expected_result);
  CHECK(result.first == counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible()));
}

TEST_CASE( "Or", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("a || b");

  counting_semantics::trace t1({0, 1, 2, 3, 4, 5});
  t1.add_signal("a", {0, 0, 0, 1, 1, 1});
  t1.add_signal("b", {0, 0, 0, 0, 0, 0});

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t1, 0).first;
  auto expected_result_0 = evaluator.evaluate_formula(spot::formula::ff(), t1, 0).first;
  CHECK(result_0 == expected_result_0);

  auto result_5 = evaluator.evaluate_formula(f, t1, 5).first;
  auto expected_result_5 = evaluator.evaluate_formula(spot::formula::tt(), t1, 5).first;
  CHECK(result_5 == expected_result_5);
}

TEST_CASE( "And", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("a && b");

  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  t.add_signal("a", {0, 0, 0, 1, 1, 1});
  t.add_signal("b", {0, 0, 0, 0, 0, 1});

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0).first;
  auto expected_result_0 = evaluator.evaluate_formula(spot::formula::ff(), t, 0).first;
  CHECK(result_0 == expected_result_0);

  auto result_3 = evaluator.evaluate_formula(f, t, 3).first;
  auto expected_result_3 = evaluator.evaluate_formula(spot::formula::ff(), t, 3).first;
  CHECK(result_3 == expected_result_3);

  auto result_5 = evaluator.evaluate_formula(f, t, 5).first;
  auto expected_result_5 = evaluator.evaluate_formula(spot::formula::tt(), t, 5).first;
  CHECK(result_5 == expected_result_5);
}

TEST_CASE( "X", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("Xa");

  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  std::vector<counting_semantics::bool3> signal = {0, 0, 0, 1, 1, 1};
  t.add_signal("a", signal);

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = evaluator.evaluate_formula(spot::formula::ff(), t, 0).first.increment();
  CHECK(result_0.first == expected_result_0);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = evaluator.evaluate_formula(spot::formula::tt(), t, 2).first.increment();
  CHECK(result_2.first == expected_result_2);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(0, 0).increment();
  CHECK(result_5.first == expected_result_5);
}

TEST_CASE( "U", "[ltl_evaluator]" )
{
  spot::formula f = spot::parse_formula("a U b");

  counting_semantics::trace t({0, 1, 2, 3, 4, 5});
  t.add_signal("a", {0, 0, 0, 1, 1, 1});
  t.add_signal("b", {0, 0, 0, 0, 0, 1});

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0).first;
  auto expected_result_0 = evaluator.evaluate_formula(spot::formula::ff(), t, 0).first;
  CHECK(result_0 == expected_result_0);

  auto result_5 = evaluator.evaluate_formula(f, t, 5).first;
  auto expected_result_5 = evaluator.evaluate_formula(spot::formula::tt(), t, 5).first;
  CHECK(result_5 == expected_result_5);
}

// Paper:
// Bartocci, E., Bloem, R., Nickovic, D. and Röck, F., 2018, July.
// A counting semantics for monitoring LTL specifications over finite traces.
// In International Conference on Computer Aided Verification (pp. 547-564). Springer, Cham.
// https://link.springer.com/chapter/10.1007/978-3-319-96145-3_29

TEST_CASE( "Table 2, Line 1", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("r");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(0, 0);
  CHECK(result_7.first == expected_result_7);
}

TEST_CASE( "Table 2, Line 2", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("g");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(0, 0);
  CHECK(result_7.first == expected_result_7);
}

TEST_CASE( "Table 2, Line 3", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("!r");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(counting_semantics::euint32_t::impossible(), 0);
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(0, 0);
  CHECK(result_7.first == expected_result_7);
}

TEST_CASE( "Table 2, Line 4", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("Fg");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(2, counting_semantics::euint32_t::impossible());
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(1, counting_semantics::euint32_t::impossible());
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(2, counting_semantics::euint32_t::impossible());
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(1, counting_semantics::euint32_t::impossible());
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(1, counting_semantics::euint32_t::infinite());
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::infinite());
  CHECK(result_7.first == expected_result_7);
}

TEST_CASE( "Table 2, Line 5", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("r -> Fg");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(2, counting_semantics::euint32_t::impossible());
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(2, counting_semantics::euint32_t::impossible());
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::impossible());
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(1, counting_semantics::euint32_t::infinite());
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(0, counting_semantics::euint32_t::infinite());
  CHECK(result_7.first == expected_result_7);
}

TEST_CASE( "Table 2, Line 6", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("G(r -> Fg)");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate_formula(f, t, 0);
  auto expected_result_0 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_0.first == expected_result_0);

  auto result_1 = evaluator.evaluate_formula(f, t, 1);
  auto expected_result_1 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_1.first == expected_result_1);

  auto result_2 = evaluator.evaluate_formula(f, t, 2);
  auto expected_result_2 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_2.first == expected_result_2);

  auto result_3 = evaluator.evaluate_formula(f, t, 3);
  auto expected_result_3 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_3.first == expected_result_3);

  auto result_4 = evaluator.evaluate_formula(f, t, 4);
  auto expected_result_4 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_4.first == expected_result_4);

  auto result_5 = evaluator.evaluate_formula(f, t, 5);
  auto expected_result_5 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_5.first == expected_result_5);

  auto result_6 = evaluator.evaluate_formula(f, t, 6);
  auto expected_result_6 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_6.first == expected_result_6);

  auto result_7 = evaluator.evaluate_formula(f, t, 7);
  auto expected_result_7 = counting_semantics::euint32_pair(counting_semantics::euint32_t::infinite(), counting_semantics::euint32_t::infinite());
  CHECK(result_7.first == expected_result_7);
}

/********************** Table 3 **********************/

TEST_CASE( "Table 3, Line 1", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("r");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate(f, t, 0);
  auto expected_result_0 = counting_semantics::bool5(true);
  CHECK(result_0 == expected_result_0);

  auto result_1 = evaluator.evaluate(f, t, 1);
  auto expected_result_1 = counting_semantics::bool5(false);
  CHECK(result_1 == expected_result_1);

  auto result_2 = evaluator.evaluate(f, t, 2);
  auto expected_result_2 = counting_semantics::bool5(false);
  CHECK(result_2 == expected_result_2);

  auto result_3 = evaluator.evaluate(f, t, 3);
  auto expected_result_3 = counting_semantics::bool5(true);
  CHECK(result_3 == expected_result_3);

  auto result_4 = evaluator.evaluate(f, t, 4);
  auto expected_result_4 = counting_semantics::bool5(false);
  CHECK(result_4 == expected_result_4);

  auto result_5 = evaluator.evaluate(f, t, 5);
  auto expected_result_5 = counting_semantics::bool5(false);
  CHECK(result_5 == expected_result_5);

  auto result_6 = evaluator.evaluate(f, t, 6);
  auto expected_result_6 = counting_semantics::bool5(true);
  CHECK(result_6 == expected_result_6);

  auto result_7 = evaluator.evaluate(f, t, 7);
  auto expected_result_7 = counting_semantics::bool5::inconclusive();
  CHECK(result_7 == expected_result_7);
}

TEST_CASE( "Table 3, Line 2", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("g");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate(f, t, 0);
  auto expected_result_0 = counting_semantics::bool5(false);
  CHECK(result_0 == expected_result_0);

  auto result_1 = evaluator.evaluate(f, t, 1);
  auto expected_result_1 = counting_semantics::bool5(false);
  CHECK(result_1 == expected_result_1);

  auto result_2 = evaluator.evaluate(f, t, 2);
  auto expected_result_2 = counting_semantics::bool5(true);
  CHECK(result_2 == expected_result_2);

  auto result_3 = evaluator.evaluate(f, t, 3);
  auto expected_result_3 = counting_semantics::bool5(false);
  CHECK(result_3 == expected_result_3);

  auto result_4 = evaluator.evaluate(f, t, 4);
  auto expected_result_4 = counting_semantics::bool5(false);
  CHECK(result_4 == expected_result_4);

  auto result_5 = evaluator.evaluate(f, t, 5);
  auto expected_result_5 = counting_semantics::bool5(true);
  CHECK(result_5 == expected_result_5);

  auto result_6 = evaluator.evaluate(f, t, 6);
  auto expected_result_6 = counting_semantics::bool5(false);
  CHECK(result_6 == expected_result_6);

  auto result_7 = evaluator.evaluate(f, t, 7);
  auto expected_result_7 = counting_semantics::bool5::inconclusive();
  CHECK(result_7 == expected_result_7);
}

TEST_CASE( "Table 3, Line 3", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("Fg");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate(f, t, 0);
  auto expected_result_0 = counting_semantics::bool5(true);
  CHECK(result_0 == expected_result_0);

  auto result_1 = evaluator.evaluate(f, t, 1);
  auto expected_result_1 = counting_semantics::bool5(true);
  CHECK(result_1 == expected_result_1);

  auto result_2 = evaluator.evaluate(f, t, 2);
  auto expected_result_2 = counting_semantics::bool5(true);
  CHECK(result_2 == expected_result_2);

  auto result_3 = evaluator.evaluate(f, t, 3);
  auto expected_result_3 = counting_semantics::bool5(true);
  CHECK(result_3 == expected_result_3);

  auto result_4 = evaluator.evaluate(f, t, 4);
  auto expected_result_4 = counting_semantics::bool5(true);
  CHECK(result_4 == expected_result_4);

  auto result_5 = evaluator.evaluate(f, t, 5);
  auto expected_result_5 = counting_semantics::bool5(true);
  CHECK(result_5 == expected_result_5);

  auto result_6 = evaluator.evaluate(f, t, 6);
  auto expected_result_6 = counting_semantics::bool5::presumably_true();
  CHECK(result_6 == expected_result_6);

  auto result_7 = evaluator.evaluate(f, t, 7);
  auto expected_result_7 = counting_semantics::bool5::presumably_true();
  CHECK(result_7 == expected_result_7);
}

TEST_CASE( "Table 3, Line 4", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("r -> Fg");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate(f, t, 0);
  auto expected_result_0 = counting_semantics::bool5(true);
  CHECK(result_0 == expected_result_0);

  auto result_1 = evaluator.evaluate(f, t, 1);
  auto expected_result_1 = counting_semantics::bool5(true);
  CHECK(result_1 == expected_result_1);

  auto result_2 = evaluator.evaluate(f, t, 2);
  auto expected_result_2 = counting_semantics::bool5(true);
  CHECK(result_2 == expected_result_2);

  auto result_3 = evaluator.evaluate(f, t, 3);
  auto expected_result_3 = counting_semantics::bool5(true);
  CHECK(result_3 == expected_result_3);

  auto result_4 = evaluator.evaluate(f, t, 4);
  auto expected_result_4 = counting_semantics::bool5(true);
  CHECK(result_4 == expected_result_4);

  auto result_5 = evaluator.evaluate(f, t, 5);
  auto expected_result_5 = counting_semantics::bool5(true);
  CHECK(result_5 == expected_result_5);

  auto result_6 = evaluator.evaluate(f, t, 6);
  auto expected_result_6 = counting_semantics::bool5::presumably_true();
  CHECK(result_6 == expected_result_6);

  auto result_7 = evaluator.evaluate(f, t, 7);
  auto expected_result_7 = counting_semantics::bool5::presumably_true();
  CHECK(result_7 == expected_result_7);
}

TEST_CASE( "Table 3, Line 5", "[ltl_evaluator]" )
{
  counting_semantics::trace t({0, 1, 2, 3, 4, 5, 6});
  t.add_signal("r", {1, 0, 0, 1, 0, 0, 1});
  t.add_signal("g", {0, 0, 1, 0, 0, 1, 0});

  spot::formula f = spot::parse_formula("G(r -> Fg)");

  counting_semantics::ltl_evaluator evaluator;
  auto result_0 = evaluator.evaluate(f, t, 0);
  auto expected_result_0 = counting_semantics::bool5::presumably_true();
  CHECK(result_0 == expected_result_0);

  auto result_1 = evaluator.evaluate(f, t, 1);
  auto expected_result_1 = counting_semantics::bool5::presumably_true();
  CHECK(result_1 == expected_result_1);

  auto result_2 = evaluator.evaluate(f, t, 2);
  auto expected_result_2 = counting_semantics::bool5::presumably_true();
  CHECK(result_2 == expected_result_2);

  auto result_3 = evaluator.evaluate(f, t, 3);
  auto expected_result_3 = counting_semantics::bool5::presumably_true();
  CHECK(result_3 == expected_result_3);

  auto result_4 = evaluator.evaluate(f, t, 4);
  auto expected_result_4 = counting_semantics::bool5::presumably_true();
  CHECK(result_4 == expected_result_4);

  auto result_5 = evaluator.evaluate(f, t, 5);
  auto expected_result_5 = counting_semantics::bool5::presumably_true();
  CHECK(result_5 == expected_result_5);

  auto result_6 = evaluator.evaluate(f, t, 6);
  auto expected_result_6 = counting_semantics::bool5::presumably_true();
  CHECK(result_6 == expected_result_6);

  auto result_7 = evaluator.evaluate(f, t, 7);
  auto expected_result_7 = counting_semantics::bool5::presumably_true();
  CHECK(result_7 == expected_result_7);
}

// TODO: add test for "G( ( ( ( ( flit_id__1___ ) U ( rst ) ) ) -> ( Eport ) ) )" and initialized wave dump

std::vector<std::tuple<unsigned, unsigned, unsigned, unsigned, unsigned, unsigned>> shayan(const std::vector<spot::formula>& formulas, const counting_semantics::trace& t)
{
  std::vector<std::tuple<unsigned, unsigned, unsigned, unsigned, unsigned, unsigned>> result;
  result.reserve(formulas.size());

  for (const auto &f : formulas)
  {
    unsigned tt = 0;
    unsigned tf = 0;
    unsigned ft = 0;
    unsigned ff = 0;
    unsigned inconclusive = 0;
    unsigned presumably = 0;

    auto splitted = counting_semantics::split_ltl(f);

    counting_semantics::ltl_evaluator evaluator;
    auto results_1 = evaluator.evaluate(splitted.first, t);
    auto results_2 = evaluator.evaluate(splitted.second, t);
    REQUIRE(results_1.size() == t.size());
    REQUIRE(results_2.size() == t.size());

    for (int i = 0; i < t.size(); ++i)
    {
      const auto &result_1 = results_1[i];
      if (result_1 == counting_semantics::bool5::inconclusive())
      {
        inconclusive++;
        continue;
      }
      const auto &result_2 = results_2[i];
      if (result_2 == counting_semantics::bool5::inconclusive())
      {
        inconclusive++;
        continue;
      }
      if (result_1 == counting_semantics::bool5::presumably_true() || result_1 == counting_semantics::bool5::presumably_false() || result_2 == counting_semantics::bool5::presumably_true() || result_2 == counting_semantics::bool5::presumably_false())
      {
        presumably++;
      }
      if (to_bool(result_1) && to_bool(result_2))
      {
        tt++;
      }
      else if (to_bool(result_1) && !to_bool(result_2))
      {
        tf++;
      }
      else if (!to_bool(result_1) && to_bool(result_2))
      {
        ft++;
      }
      else//if (!to_bool(result_1) && !to_bool(result_2))
      {
        ff++;
      }
    }
    result.emplace_back(std::tie(tt, tf, ft, ff, inconclusive, presumably));
  }

  return result;
}

TEST_CASE( "Basic - Shayan functionality", "[shayan]" )
{
  std::string formula = "G( ( ( ( ( !( rst ) ) U ( !( cur_addr_rst__3___ ) ) ) ) -> ( !( Nport ) ) ) )";
  std::vector<spot::formula> formulas;
  formulas.emplace_back(spot::parse_formula(formula));

  std::stringstream ss;
  ss << TEST_LOCATION << "/vcd_files/wave_dumps_real_scenario_initialized.vcd";
  std::string filename = ss.str();
  counting_semantics::vcd_parser parser(filename);
  auto scopes = parser.get_scopes();
  auto signals = parser.get_signals(scopes[0]);
  int j = 1;
  while (signals.empty())
  {
    signals = parser.get_signals(scopes[j++]);
  }
  auto t = parser.to_trace(signals);
  REQUIRE(!t.empty());

  auto result = shayan(formulas, t);
  REQUIRE(result.size() == formulas.size());
  REQUIRE(result.size() == 1);

  auto [tt, tf, ft, ff, inconclusive, presumably] = result[0];
//  CHECK(tt == 64);
//  CHECK(tf == 0);
//  CHECK(ft == 88);
//  CHECK(ff == 0);
  CHECK(inconclusive == 0);
  CHECK(presumably == 0);

//  BENCHMARK("Shayan") {
//    return shayan(formulas, t);
//  };

//  ss.str(std::string());
//  ss.clear();
//  ss << TEST_LOCATION << "/ltl_properties/benchmark.txt";
//  std::string ltl_file = ss.str();
//  std::ifstream file_in(ltl_file);
//
//  std::string line;
//  while (std::getline(file_in, line))
//  {
//    formulas.emplace_back(spot::parse_formula(line));
//  }
//
//  BENCHMARK("Shayan") {
//    return shayan(formulas, t);
//  };
}
