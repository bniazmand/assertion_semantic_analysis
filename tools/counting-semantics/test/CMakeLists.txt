include_directories(catch2)

file(GLOB_RECURSE FILENAMES *.cpp)

add_executable(run_tests ${FILENAMES})
target_link_libraries(run_tests counting_semantics Catch2)
target_compile_definitions(run_tests PRIVATE TEST_LOCATION="${PROJECT_SOURCE_DIR}/test")