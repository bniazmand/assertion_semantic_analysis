//
// Created by Gianluca on 30/11/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#define CATCH_CONFIG_MAIN
#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch.hpp>