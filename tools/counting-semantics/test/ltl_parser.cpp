//
// Created by Gianluca on 30/11/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#include <fstream>
#include <catch2/catch.hpp>
#include <spot/tl/parse.hh>

TEST_CASE( "Sanity check", "[ltl_parser]" )
{
  CHECK_THROWS(spot::parse_formula("[]<>p0 || <>[p1"));
}

TEST_CASE( "Default LTL formula", "[ltl_parser]" )
{
  CHECK_NOTHROW(spot::parse_formula("[]<>p0 || <>[]p1"));
}

TEST_CASE("Simple generated LTL property", "[ltl_parser]")
{
  CHECK_NOTHROW(spot::parse_formula("G( ( ( !( Lreq=1 ) ) -> ( F( G( nextstate__0_=1 ) ) ) ) )"));
}

TEST_CASE("Generated LTL properties from file", "[ltl_parser]")
{
  std::ifstream infile(std::string(TEST_LOCATION) + "/ltl_properties/arbiter.txt");
  REQUIRE(infile.is_open());
  std::string line;
  while (std::getline(infile, line)) {
    CHECK_NOTHROW(spot::parse_formula(line));
  }
}