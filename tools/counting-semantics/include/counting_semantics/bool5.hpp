//
// Created by Gianluca on 01/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <string>

namespace counting_semantics
{

class bool5 {
public:
  struct presumably_false_t {};
  struct inconclusive_t {};
  struct presumably_true_t {};

  static bool5 presumably_false() {
    return bool5{presumably_false_t{}};
  }

  static bool5 inconclusive() {
    return bool5{inconclusive_t{}};
  }

  static bool5 presumably_true() {
    return bool5{presumably_true_t{}};
  }

public:
  bool5() = default;

  bool5( bool b )
      : value( b ? true_value : false_value )
  {
  }

  explicit bool5( presumably_false_t const& )
      : value( presumably_false_value )
  {
  }

  explicit bool5( inconclusive_t const& )
      : value( inconclusive_value )
  {
  }

  explicit bool5( presumably_true_t const& )
      : value( presumably_true_value )
  {
  }

  bool operator==( bool5 const& other ) const
  {
    return value == other.value;
  }

  bool operator!=( bool5 const& other ) const
  {
    return !this->operator==( other );
  }

  bool operator<( bool5 const& other ) const
  {
    return value < other.value;
  }

  bool operator>( bool5 const& other ) const
  {
    return value > other.value;
  }

  bool operator<=( bool5 const& other ) const
  {
    return !operator>( other );
  }

  bool operator>=( bool5 const& other ) const
  {
    return !operator<( other );
  }

  bool5 operator!() const
  {
    switch ( value )
    {
      default:
      case false_value:
        return true;
      case presumably_false_value:
        return presumably_true();
      case inconclusive_value:
        return inconclusive();
      case presumably_true_value:
        return presumably_false();
      case true_value:
        return false;
    }
  }

  bool5 operator&( bool5 const& other ) const
  {
    return std::min( *this, other );
  }

  bool5 operator&&( bool5 const& other ) const
  {
    if ( is_false() )
    {
      return false;
    }
    else if ( is_presumably_false() )
    {
      return bool5( presumably_false_t() );
    }
    return ( *this & other );
  }

  bool5 operator|( bool5 const& other ) const
  {
    return std::max( *this, other );
  }

  bool5 operator||( bool5 const& other ) const
  {
    if ( is_true() )
    {
      return true;
    }
    else if ( is_presumably_true() )
    {
      return bool5( presumably_true_t() );
    }
    return ( *this | other );
  }

  bool is_false() const
  {
    return ( value == false_value );
  }

  bool is_presumably_false() const
  {
    return ( value == presumably_false_value );
  }

  bool is_inconclusive() const
  {
    return ( value == inconclusive_value );
  }

  bool is_presumably_true() const
  {
    return ( value == presumably_true_value );
  }

  bool is_true() const
  {
    return ( value == true_value );
  }

  std::string to_string() const
  {
    switch ( value )
    {
      default:
      case inconclusive_value:
        return "?";
      case true_value:
        return "T";
      case presumably_true_value:
        return "Tp";
      case presumably_false_value:
        return "Fp";
      case false_value:
        return "F";
    }
  }

protected:
  enum value_t {
    false_value = 0,
    presumably_false_value = 1,
    inconclusive_value = 2,
    presumably_true_value = 3,
    true_value = 4,
  } value = false_value;
}; /* bool5 */

std::ostream& operator<<(std::ostream& os, const bool5& value)
{
  os << value.to_string();
  return os;
}

}