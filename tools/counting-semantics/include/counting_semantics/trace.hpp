//
// Created by Gianluca on 01/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <counting_semantics/bool3.hpp>

#include <fmt/ranges.h>
#include <spdlog/spdlog.h>

#include <cassert>
#include <deque>
#include <unordered_map>
#include <utility>
#include <vector>

namespace counting_semantics
{

//enum class signal_state {
//  low,
//  high,
//  Z
//
//};

class trace {

public:
  explicit trace(std::deque<double> timestamps) : _timestamps(std::move(timestamps)) {}

  void add_signal(const std::string &name, const std::vector<bool3> &t) {
    if (t.empty()) {
      throw std::runtime_error("Adding an empty signal to the trace.");
    }
    else if (size() == 0)
    {
      _trace.emplace(name, t);
    }
    else if (t.size() == size())
    {
      _trace.emplace(name, t);
    }
    else
    {
      throw std::runtime_error(fmt::format("Adding a signal of different length to the trace.\nExisting: {}, New: {} ({})", size(), name, t.size()));
    }
  }

  const std::vector<bool3> &get_signal(const std::string &name) const {
    return _trace.at(name);
  }

  std::size_t size() const {
    return _timestamps.size();
  }

  bool empty() const {
    return size() == 0;
  }

  trace negative_edge(const std::string& clock_signal_name) {
    auto t_clock = _trace.at(clock_signal_name);
    std::deque<double> timestamps;
    assert(!t_clock.empty());

    std::vector<int> selected;
    auto last_value = t_clock[0];

    if (t_clock[0].is_false()) {
      timestamps.emplace_back(_timestamps[0]);
      selected.emplace_back(0);
    }

    for (auto i = 1u; i < t_clock.size(); i++ ) {
      if (t_clock[i] != last_value && t_clock[i].is_false()) {
        timestamps.emplace_back(_timestamps[i]);
        selected.emplace_back(i);
      }
      last_value = t_clock[i];
    }

    spdlog::debug("Selected cycles: {}", selected);

    trace t(timestamps);
    std::vector<bool3> s;
    s.reserve(timestamps.size());
    for (const auto &signal : _trace) {
      s.clear();
      for (const auto &index : selected) {
        s.emplace_back(signal.second[index]);
      }
      t.add_signal(signal.first, s);
    }

    return t;
  }

  trace positive_edge(const std::string& clock_signal_name) {
    auto t_clock = _trace.at(clock_signal_name);
    std::deque<double> timestamps;
    assert(!t_clock.empty());

    std::vector<int> selected;
    auto last_value = t_clock[0];

    if (t_clock[0].is_true()) {
      timestamps.emplace_back(_timestamps[0]);
      selected.emplace_back(0);
    }

    for (auto i = 1u; i < t_clock.size(); i++ ) {
      if (t_clock[i] != last_value && t_clock[i].is_true()) {
        timestamps.emplace_back(_timestamps[i]);
        selected.emplace_back(i);
      }
      last_value = t_clock[i];
    }

    spdlog::debug("Selected cycles: {}", selected);

    trace t(timestamps);
    std::vector<bool3> s;
    s.reserve(timestamps.size());
    for (const auto &signal : _trace) {
      s.clear();
      for (const auto &index : selected) {
        s.emplace_back(signal.second[index]);
      }
      t.add_signal(signal.first, s);
    }

    return t;
  }

  void trim(int timesteps_from_start, int timesteps_from_end = 0) {

    if (timesteps_from_start < 0 || timesteps_from_start >= size()) {
      throw std::runtime_error(fmt::format("timesteps_from_start: {}, size: {}", timesteps_from_start, size()));
    }
    if (timesteps_from_end < 0 || timesteps_from_end >= size()) {
      throw std::runtime_error(fmt::format("timesteps_from_end: {}, size: {}", timesteps_from_end, size()));
    }

    auto num = size() - timesteps_from_end;

    for (auto &item : _trace)
    {
      item.second.erase(item.second.begin(), item.second.begin() + timesteps_from_start);
      if (timesteps_from_end != 0) {
        item.second.erase(item.second.begin() + num, item.second.end());
      }
    }

    _timestamps.erase(_timestamps.begin(), _timestamps.begin() + timesteps_from_start);
    if (timesteps_from_end != 0) {
      _timestamps.erase(_timestamps.begin() + num, _timestamps.end());
    }
  }

protected:
  std::deque<double> _timestamps;
  std::unordered_map<std::string, std::vector<bool3>> _trace;
};

}// namespace counting_semantics