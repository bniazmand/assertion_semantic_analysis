//
// Created by Gianluca on 17/03/2021.
// Copyright (c) 2021 TUHH. All rights reserved.
//

#pragma once

#include <counting_semantics/bool5.hpp>

namespace counting_semantics
{

bool to_bool(counting_semantics::bool5 val) {
  if (val == counting_semantics::bool5::inconclusive())
  {
    throw std::runtime_error("You should not pass inconclusive to this function");
  }
  if (val == counting_semantics::bool5::presumably_true() || val == counting_semantics::bool5(true))
  {
    return true;
  }
  else
  {
    return false;
  }
}

}