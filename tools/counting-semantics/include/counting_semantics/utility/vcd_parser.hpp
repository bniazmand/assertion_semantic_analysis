//
// Created by Gianluca on 04/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <counting_semantics/trace.hpp>
#include <counting_semantics/bool3.hpp>

#include <vcd-parser/VCDFileParser.hpp>
#include <spdlog/spdlog.h>

#include <functional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace counting_semantics
{

std::string to_string(VCDSignal *value) {
  std::stringstream ss;
  ss << value->reference;
  if (value->size > 1)
  {
    ss << "[" << value->size - 1 << ":0]";
  }
  return ss.str();
}

template<typename InputIt>
std::string join(InputIt begin, InputIt end, const std::string &separator = ", ", const std::string &concluder = "", std::function<std::string(InputIt)> element_printer = [](InputIt it){ std::stringstream ss; ss << *it; return ss.str(); }) {
  std::ostringstream ss;

  if (begin != end)
  {
    ss << element_printer(begin++);
  }

  while (begin != end)
  {
    ss << separator;
    ss << element_printer(begin++);
  }

  ss << concluder;
  return ss.str();
}

std::string join(const std::vector<VCDSignal *> &signals, const std::string &separator = ", ", const std::string &concluder = "") {
  std::vector<std::string> names;
  names.reserve(signals.size());
  std::unordered_map<std::string, int> occurrences;
  occurrences.reserve(signals.size());
  for (auto signal : signals)
  {
    names.emplace_back(to_string(signal));
    if (!occurrences.emplace(names.back(), 1).second) {
      occurrences.at(names.back())++;
    }
  }

  auto printer = [](std::unordered_map<std::string, int>::const_iterator it){
    if (it->second == 1) {
      return it->first;
    }
    else {
      return fmt::format("{}[{}:0]", it->first, it->second - 1);
    }
  };

  return join<std::unordered_map<std::string, int>::const_iterator>(occurrences.cbegin(), occurrences.cend(), separator, concluder, printer);
}

class vcd_parser {

public:
  explicit vcd_parser(const std::string &filename) {
    _trace = _parser.parse_file(filename);
    if (_trace == nullptr)
    {
      throw std::runtime_error("Unreadable file.");
    }
  }

  [[nodiscard]] std::vector<std::string> get_scopes() const {
    std::vector<std::string> scopes;

    for (VCDScope *scope : *_trace->get_scopes())
    {
      if (scope->name.empty())
      {
        continue;
      }
      scopes.emplace_back(scope->name);
    }

    return scopes;
  }

  [[nodiscard]] std::vector<VCDSignal *> get_signals(const std::string &scope_name) const {
    auto *scope = _trace->get_scope(scope_name);
    if (scope == nullptr)
    {
      throw std::runtime_error("The selected scope does not exist: " + scope_name);
    }

    return scope->signals;
  }

  [[nodiscard]] std::deque<VCDTime> get_timestamps() const {
    auto t = _trace->get_timestamps();
    return std::deque<VCDTime>(t->begin(), t->end());
  }

  // DO NOT USE
  //  VCDValue get_signal_value_at(const std::string& hash, VCDTime time) {
  //    auto* val = _trace->get_signal_value_at(hash, time);
  //    if (val == nullptr) {
  //      throw std::runtime_error(fmt::format("Nullptr value for signal () at time: ()", hash, time));
  //    }
  //    return *_trace->get_signal_value_at(hash, time);
  //  }

  [[nodiscard]] std::vector<bool3> to_bool_vector(VCDValue *value, std::size_t size) const {
    std::vector<bool3> s;

    if (value->get_type() == VCD_SCALAR)
    {
      auto val = value->get_value_bit();
      bool3 b;

      if (val == VCD_X || val == VCD_Z)
      {
        b = bool3::inconclusive();
      }
      if (val == VCD_1)
      {
        b = true;
      }
      s.push_back(b);
    }
    else if (value->get_type() == VCD_VECTOR)
    {
      VCDBitVector *vecval = value->get_value_vector();
      s.reserve(size);

      for (int i = vecval->size() - 1; i >= 0; --i)
      {
        bool3 b;
        if ((*vecval)[i] == VCD_X || (*vecval)[i] == VCD_Z)
        {
          b = bool3::inconclusive();
        }
        if ((*vecval)[i] == VCD_1)
        {
          b = true;
        }
        s.emplace_back(b);
      }

      for (auto i = vecval->size(); i < size; ++i)
      {
        s.emplace_back(false);
      }
    }
    else if (value->get_type() == VCD_REAL)
    {
      throw std::runtime_error("Not yet implemented");
    }

    return s;
  }

  [[nodiscard]] std::vector<bool3> to_signal(VCDSignal *signal, uint32_t bit = 0) const {
    std::vector<bool3> s;

    auto timestamps = get_timestamps();
    assert(!timestamps.empty());

    if (timestamps.size() > std::numeric_limits<int>::max()) {
      throw std::runtime_error("The trace contains too many timesteps");
    }

    s.reserve(timestamps.size());

    auto *values = _trace->get_signal_values(signal->hash);
    assert(!values->empty());

    for (int i = 0; i < timestamps.size(); ++i)
    {
      VCDValue *value;

      if (values->size() == timestamps.size())
      {
        value = (*values)[i]->value;
      }
      else
      {
        value = _trace->get_signal_value_at(signal->hash, timestamps[i]);
      }

      auto bool_vector = to_bool_vector(value, signal->size);
      assert(bool_vector.size() == signal->size);

      if (signal->size == 1)
      {
        s.push_back(bool_vector[0]);
      }
      else
      {
        s.push_back(bool_vector[bit]);
      }
    }

    return s;
  }

  [[nodiscard]] trace to_trace(const std::vector<VCDSignal *> &signals) const {
    auto timestamps = get_timestamps();
    trace t(timestamps);

    for (const auto &signal : signals)
    {
      if (signal->lindex != -1 && signal->size == 1) {
        t.add_signal(fmt::format("{}__{}___", signal->reference, signal->lindex), to_signal(signal));
      }
      else if (signal->size == 1) {
        t.add_signal(signal->reference, to_signal(signal));
      }
      else if (signal->size > 1) {
        for (int i = 0; i < signal->size; ++i)
        {
          t.add_signal(fmt::format("{}__{}___", signal->reference, i), to_signal(signal, i));
        }
      }
      else {
        throw std::runtime_error("Unexpected signal format");
      }
    }

    return t;
  }

protected:
  VCDFileParser _parser;
  VCDFile *_trace;
};

}// namespace counting_semantics