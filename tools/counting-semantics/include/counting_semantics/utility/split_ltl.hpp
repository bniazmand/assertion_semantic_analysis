//
// Created by Gianluca on 17/03/2021.
// Copyright (c) 2021 TUHH. All rights reserved.
//

#pragma once

#include <spot/tl/parse.hh>

namespace counting_semantics
{

std::pair<spot::formula, spot::formula> split_ltl(const spot::formula &f) {
  assert(f.is(spot::op::G));
  auto inner = *f.begin();
  assert(inner.is(spot::op::Implies));
  auto first = *inner.begin();
  auto second = *(++inner.begin());
  return {first, second};
}

}