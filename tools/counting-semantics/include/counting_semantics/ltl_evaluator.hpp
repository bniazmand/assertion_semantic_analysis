//
// Created by Gianluca on 01/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <counting_semantics/bool3.hpp>
#include <counting_semantics/bool5.hpp>
#include <counting_semantics/extended_inttype.hpp>
#include <counting_semantics/trace.hpp>

#include <spot/tl/formula.hh>
#include <spdlog/spdlog.h>
#include <fmt/ostream.h>

#include <array>
#include <iostream>

namespace counting_semantics
{

class ltl_evaluator {
public:
  using evaluation_result = std::pair<euint32_pair, spot::formula>;

  // Primary formulations
  [[nodiscard]] evaluation_result evaluate_variable(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::ap));

    if (pos >= t.size())
    {
      return {euint32_pair(0, 0), f};
    }
    else
    {
      const auto &signal = t.get_signal(f.ap_name());
      if (signal[pos].is_true())
      {
        return {euint32_pair(0, euint32_t::impossible()), f};
      }
      else if (signal[pos].is_false())
      {
        return {euint32_pair(euint32_t::impossible(), 0), f};
      }
      else {
        throw std::runtime_error(fmt::format("Unexpected value in signal {} at position {}", f, pos));
      }
    }
  }

  [[nodiscard]] evaluation_result evaluate_not(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::Not) && f.size() == 1);

    return {evaluate_formula(*(f.begin()), t, pos).first.swap(), f};
  }

  [[nodiscard]] evaluation_result evaluate_or(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::Or) && f.size() == 2);

    return {evaluate_formula(*f.begin(), t, pos).first.minmax(evaluate_formula(*(++f.begin()), t, pos).first), f};
  }

  [[nodiscard]] evaluation_result evaluate_next(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::X) && f.size() == 1);

    return {evaluate_formula(*f.begin(), t, pos + 1).first.increment(), f};
  }

  [[nodiscard]] evaluation_result evaluate_until(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::U) && f.size() == 2);

    if (pos >= t.size())
    {
      return {evaluate_formula(*(++f.begin()), t, pos).first.minmax(evaluate_formula(*(f.begin()), t, pos).first.maxmin(euint32_pair(euint32_t::impossible(), euint32_t::infinite()))), f};
    }
    else
    {
      return {evaluate_formula(*(++f.begin()), t, pos).first.minmax(evaluate_formula(*(f.begin()), t, pos).first.maxmin(evaluate_until(f, t, pos + 1).first)), f};
    }
  }

  [[nodiscard]] evaluation_result evaluate_eventually(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::F) && f.size() == 1);

    if (pos >= t.size())
    {
      return {evaluate_formula(*(f.begin()), t, pos).first.minmax(euint32_pair(euint32_t::impossible(), euint32_t::infinite())), f};
    }
    else
    {
      return {evaluate_formula(*(f.begin()), t, pos).first.minmax(evaluate_eventually(f, t, pos + 1).first.increment()), f};
    }
  }

  // Derived formulations

  [[nodiscard]] evaluation_result evaluate_and(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::And) && f.size() == 2);

    spot::formula reformulation = spot::formula::Not(spot::formula::Or({spot::formula::Not(*f.begin()), spot::formula::Not(*(++f.begin()))}));

    return {evaluate_formula(reformulation, t, pos).first, reformulation};
  }

  [[nodiscard]] evaluation_result evaluate_globally(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::G) && f.size() == 1);

    spot::formula reformulation = spot::formula::Not(spot::formula::F(spot::formula::Not(*f.begin())));

    return {evaluate_formula(reformulation, t, pos).first, reformulation};
  }

  [[nodiscard]] evaluation_result evaluate_implies(const spot::formula &f, const trace &t, std::size_t pos) const {
    assert(f.is(spot::op::Implies) && f.size() == 2);

    spot::formula reformulation = spot::formula::Or({(spot::formula::Not(*f.begin())), *(++f.begin())});

    return {evaluate_formula(reformulation, t, pos).first, reformulation};
  }

  [[nodiscard]] evaluation_result evaluate_formula(const spot::formula &f, const trace &t, std::size_t pos) const {
    SPDLOG_TRACE("[EvF] Formula: {}, Pos: {}", f, pos);

    if (f.is(spot::op::ff))
    {
      return {euint32_pair(euint32_t::impossible(), 0), f};
    }
    else if (f.is(spot::op::tt))
    {
      auto result = evaluate_formula(spot::formula::unop(spot::op::Not, f), t, pos);
      return {result.first.swap(), result.second};
    }
    else if (f.is(spot::op::ap))
    {
      return evaluate_variable(f, t, pos);
    }
    else if (f.is(spot::op::Not))
    {
      return evaluate_not(f, t, pos);
    }
    else if (f.is(spot::op::Or))
    {
      return evaluate_or(f, t, pos);
    }
    else if (f.is(spot::op::And))
    {
      return evaluate_and(f, t, pos);
    }
    else if (f.is(spot::op::X))
    {
      return evaluate_next(f, t, pos);
    }
    else if (f.is(spot::op::U))
    {
      return evaluate_until(f, t, pos);
    }
    else if (f.is(spot::op::F))
    {
      return evaluate_eventually(f, t, pos);
    }
    else if (f.is(spot::op::G))
    {
      return evaluate_globally(f, t, pos);
    }
    else if (f.is(spot::op::Implies))
    {
      return evaluate_implies(f, t, pos);
    }

    throw std::runtime_error("Unknown operator in main evaluation");
  }

  [[nodiscard]] bool3 prediction_predicate(const spot::formula &f, const trace &t, std::size_t pos, euint32_pair result_pos) const {

    uint32_t max_s_prime = 0;

    for (int j = pos - 1; j >= 0; --j) {
      auto result = evaluate_formula(f, t, j).first;

      if (result.s.is_normal() && result.f.is_impossible()) {
        if (max_s_prime < result.s.value) {
          max_s_prime = result.s.value;
        }

        if (result_pos.s.value <= result.s.value) {
          return bool3{true};
        }
      }
    }

    if (result_pos.s.value > max_s_prime) {
      return bool3{false};
    }

    return bool3::inconclusive();
  }

  [[nodiscard]] bool5 evaluate_auxiliary(const spot::formula &f, const trace &t, std::size_t pos) const {
    SPDLOG_TRACE("[Aux] Formula: {}, Pos: {}", f, pos);

    if (f.is(spot::op::ap)) {
      return bool5::inconclusive();
    }
    else if (f.is(spot::op::Not))
    {
      assert(f.size() == 1);
      auto e_pi = evaluate(*f.begin(), t, pos);
      SPDLOG_TRACE("[Aux] e_pi({}, {}) = {} returning {}", *f.begin(), pos, e_pi, !e_pi);
      return !e_pi;
    }
    else if (f.is(spot::op::Or))
    {
      assert(f.size() == 2);
      return evaluate(*f.begin(), t, pos) || evaluate(*(++f.begin()), t, pos);
    }
    else if (f.is(spot::op::X))
    {
      assert(f.size() == 1);
      return evaluate(*f.begin(), t, pos + 1);
    }
    else if (f.is(spot::op::F))
    {
      assert(f.size() == 1);

      if (pos <= t.size())
      {
        SPDLOG_TRACE("[Aux] Calculating: e_pi({}, {}) OR r_pi({}, {})", *f.begin(), pos, spot::formula::X(spot::formula::F(*f.begin())), pos);
        auto e_pi = evaluate(*f.begin(), t, pos);
        SPDLOG_TRACE("[Aux] e_pi({}, {}) = {}", *f.begin(), pos, e_pi);
        auto r_pi = evaluate_auxiliary(spot::formula::X(spot::formula::F(*f.begin())),t ,pos);
        SPDLOG_TRACE("[Aux] r_pi({}, {}) = {}", spot::formula::X(spot::formula::F(*f.begin())), pos, r_pi);
        SPDLOG_TRACE("[Aux] result = {}", e_pi || r_pi);
        return e_pi || r_pi;
      }
      else
      {
        SPDLOG_TRACE("[Aux] Calculating: e_pi({}, {})", *f.begin(), pos);
        auto e_pi = evaluate(*f.begin(), t, pos);
        SPDLOG_TRACE("[Aux] e_pi({}, {}) = {}", *f.begin(), pos, e_pi);
        return e_pi;
      }
    }
    else if (f.is(spot::op::U))
    {
      assert(f.size() == 2);

      if (pos <= t.size())
      {
        return evaluate(*(++f.begin()), t, pos) || (evaluate(*(++f.begin()), t, pos) && evaluate_auxiliary(spot::formula::X(spot::formula::U(*f.begin(), *(++f.begin()))),t ,pos));
      }
      else
      {
        return evaluate(*(++f.begin()), t, pos);
      }
    }
    else if (f.is(spot::op::G))
    {
      assert(f.size() == 1);

      spot::formula reformulation = spot::formula::Not(spot::formula::F(spot::formula::Not(*f.begin())));

      SPDLOG_TRACE("[Aux] {} reformulated to {}", f, reformulation);

      return evaluate_auxiliary(reformulation, t, pos);
    }

    throw std::runtime_error("Unknown operator in auxiliary evaluation");
  }

  [[nodiscard]] bool5 evaluate(const spot::formula &formula, const trace &t, std::size_t pos) const {

    SPDLOG_TRACE("Formula: {}, Pos: {}", formula, pos);
    auto result = evaluate_formula(formula, t, pos);
    auto prediction = result.first;
    auto f = result.second;
    SPDLOG_TRACE("d_pi = {}", prediction);

    if (prediction.s.is_normal() && prediction.f.is_impossible())
    {
      return bool5(true);
    }
    else if (prediction.s.is_normal() && prediction.f.is_normal())
    {
      auto prediction_not = evaluate_formula(spot::formula::Not(f), t, pos);
      auto pp = prediction_predicate(f, t, pos, prediction);
      auto pp_not = prediction_predicate(spot::formula::Not(f), t, pos, prediction_not.first);

      if (pp > pp_not)
      {
        return bool5::presumably_true();
      }
      else if (pp == pp_not)
      {
        return evaluate_auxiliary(f, t, pos);
      }
      else// if (pp < pp_not)
      {
        return bool5::presumably_false();
      }
    }
    else if (prediction.s.is_normal() && prediction.f.is_infinite())
    {
      auto pp = prediction_predicate(f, t, pos, prediction);

      if (pp == bool3(true))
      {
        return bool5::presumably_true();
      }
      else if (pp == bool3::inconclusive())
      {
        return evaluate_auxiliary(f, t, pos);
      }
      else// if (pp == bool3(false))
      {
        return bool5::presumably_false();
      }
    }
    else if (prediction.s.is_infinite() && prediction.f.is_normal())
    {
      return !evaluate(spot::formula::Not(f), t, pos);
    }
    else if (prediction.s.is_infinite() && prediction.f.is_infinite())
    {
      return evaluate_auxiliary(f, t, pos);
    }
    else if (prediction.s.is_impossible() && prediction.f.is_normal())
    {
      return bool5(false);
    }

    throw std::runtime_error("We should not reach this point");
  }

  [[nodiscard]] std::vector<bool5> evaluate(const spot::formula &formula, const trace &t) {
    std::vector<bool5> tmp;
    tmp.reserve(t.size());

    SPDLOG_TRACE("Formula {}", formula);

    auto search_result = _dictionary.find(formula);
    if (search_result != _dictionary.end()) {
      SPDLOG_TRACE("Found in dictionary", formula);
      return search_result->second;
    }

    for (int i = 0; i < t.size(); ++i)
    {
      tmp.emplace_back(evaluate(formula, t, i));
    }

    auto emplace_result = _dictionary.emplace(formula, tmp);
    assert(emplace_result.second);

    return tmp;
  }

private:
  std::unordered_map<spot::formula, std::vector<bool5>> _dictionary;
};

}// namespace counting_semantics