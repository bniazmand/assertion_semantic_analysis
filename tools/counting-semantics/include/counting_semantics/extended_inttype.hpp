//
// Created by Gianluca on 02/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <fmt/format.h>

#include <algorithm>
#include <cstdint>
#include <sstream>

namespace counting_semantics
{

template<typename IntType>
class extended_inttype
{
public:
  struct infinite_t {};
  struct impossible_t {};

public:
  static extended_inttype<IntType> impossible()
  {
    return extended_inttype<IntType>( impossible_t() );
  }

  static extended_inttype<IntType> infinite()
  {
    return extended_inttype<IntType>( infinite_t() );
  }

public:
  extended_inttype( uint32_t value )
      : value( value )
      , extension( normal_value )
  {
  }

  explicit extended_inttype( infinite_t const& )
      : value( 0 )
      , extension( infinite_value )
  {
  }

  explicit extended_inttype( impossible_t const& )
      : value( 0 )
      , extension( impossible_value )
  {
  }

  bool operator==( extended_inttype<IntType> const& other ) const
  {
    return value == other.value && extension == other.extension;
  }

  bool operator!=( extended_inttype<IntType> const& other ) const
  {
    return !this->operator==( other );
  }

  bool operator<( extended_inttype<IntType> const& other ) const
  {
    /* compare values if both types are normal */
    if ( is_normal() && other.is_normal() )
    {
      return value < other.value;
    }
      /* compare extensions otherwise */
    else
    {
      return extension < other.extension;
    }
  }

  bool operator>( extended_inttype<IntType> const& other ) const
  {
    /* compare values if both types are normal */
    if ( is_normal() && other.is_normal() )
    {
      return value > other.value;
    }
      /* compare extensions otherwise */
    else
    {
      return extension > other.extension;
    }
  }

  bool operator<=( extended_inttype<IntType> const& other ) const
  {
    return !this->operator<( other );
  }

  bool operator>=( extended_inttype<IntType> const& other ) const
  {
    return !this->operator>( other );
  }

  bool is_normal() const
  {
    return extension == normal_value;
  }

  bool is_infinite() const
  {
    return extension == infinite_value;
  }

  bool is_impossible() const
  {
    return extension == impossible_value;
  }

  extended_inttype<IntType> operator+( extended_inttype<IntType> const& other ) const
  {
    if ( is_normal() && other.is_normal() )
    {
      return extended_inttype<IntType>( value + other.value );
    }

    switch ( std::max( extension, other.extension ) )
    {
      default:
      case impossible_value:
        return extended_inttype<IntType>( impossible_t() );
      case infinite_value:
        return extended_inttype<IntType>( infinite_t() );
    }
  }

public:
  IntType value;

  enum extension_t {
    normal_value = -1,
    infinite_value = 0,
    impossible_value = 1,
  } extension;
}; /* extended_inttype */

using euint32_t = extended_inttype<uint32_t>;

template<typename IntType>
class extended_inttype_pair
{
public:
  explicit extended_inttype_pair( extended_inttype<IntType> const& s, extended_inttype<IntType> const& f )
      : s( s )
      , f( f )
  {
  }

  bool operator==( extended_inttype_pair<IntType> const& other ) const
  {
    return s == other.s && f == other.f;
  }

  bool operator!=( extended_inttype_pair<IntType> const& other ) const
  {
    return !( this->operator==( other ) );
  }

  extended_inttype_pair<IntType> swap() const
  {
    return extended_inttype_pair( f, s );
  }

  extended_inttype_pair<IntType> increment() const
  {
    return extended_inttype_pair( s + 1, f + 1 );
  }

  extended_inttype_pair<IntType> minmax( extended_inttype_pair<IntType> const& other ) const
  {
    return extended_inttype_pair( std::min( s, other.s ), std::max( f, other.f ) );
  }

  extended_inttype_pair<IntType> maxmin( extended_inttype_pair<IntType> const& other ) const
  {
    return extended_inttype_pair( std::max( s, other.s ), std::min( f, other.f ) );
  }

public:
  extended_inttype<IntType> s;
  extended_inttype<IntType> f;
}; /* extended_inttype_pair */

using euint32_pair = extended_inttype_pair<uint32_t>;

std::ostream& operator<<(std::ostream& os, const euint32_t& value)
{
  std::string s;
  if (value.extension == euint32_t::normal_value) {
    s = std::to_string(value.value);
  }
  else if (value.extension == euint32_t::infinite_value) {
    s = "inf";
  }
  else {
    s = "imp";
  }

  os << s;
  return os;
}

std::ostream& operator<<(std::ostream& os, const euint32_pair& value)
{
  std::stringstream s;
  s << "(" << value.s << ", " << value.f << ")";
  os << s.str();
  return os;
}

}

