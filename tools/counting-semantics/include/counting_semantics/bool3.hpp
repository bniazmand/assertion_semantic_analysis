//
// Created by Gianluca on 03/12/2020.
// Copyright (c) 2020 TUHH. All rights reserved.
//

#pragma once

#include <string>

namespace counting_semantics
{

class bool3 {
public:
  struct inconclusive_t {};

  static bool3 inconclusive() {
    return bool3{inconclusive_t{}};
  }

public:
  bool3()
      : value(false_value) {
  }

  bool3(bool b)
      : value(b ? true_value : false_value) {
  }

  explicit bool3(inconclusive_t const &)
      : value(inconclusive_value) {
  }

  bool operator==(bool3 const &other) const {
    return value == other.value;
  }

  bool operator!=(bool3 const &other) const {
    return !this->operator==(other);
  }

  bool operator<(bool3 const &other) const {
    return value < other.value;
  }

  bool operator>(bool3 const &other) const {
    return value > other.value;
  }

  bool operator<=(bool3 const &other) const {
    return !operator>(other);
  }

  bool operator>=(bool3 const &other) const {
    return !operator<(other);
  }

  bool3 operator!() const {
    switch (value)
    {
      default:
      case inconclusive_value:
        return inconclusive();
      case false_value:
        return true;
      case true_value:
        return false;
    }
  }

  bool3 operator&(bool3 const &other) const {
    return std::min(*this, other);
  }

  bool3 operator&&(bool3 const &other) const {
    if (is_false())
      return false;

    return (*this & other);
  }

  bool3 operator|(bool3 const &other) const {
    return std::max(*this, other);
  }

  bool3 operator||(bool3 const &other) const {
    if (is_true())
      return true;

    return (*this | other);
  }

  bool is_false() const {
    return (value == false_value);
  }

  bool is_true() const {
    return (value == true_value);
  }

  bool is_inconclusive() const {
    return (value == inconclusive_value);
  }

  [[nodiscard]] std::string to_string() const {
    switch (value)
    {
      default:
      case inconclusive_value:
        return "?";
      case false_value:
        return "0";
      case true_value:
        return "1";
    }
  }

protected:
  enum value_t {
    false_value = -1,
    inconclusive_value = 0,
    true_value = 1,
  } value;
}; /* bool3 */

inline std::ostream& operator<<(std::ostream& os, const bool3& value)
{
  os << value.to_string();
  return os;
}

} /* namespace counting_semantics */